package edu;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class XMLRouteParser {

    public static void main(String[] args) {

        SAXParserFactory factory = SAXParserFactory.newInstance();

        XMLRouteHandler handler = new XMLRouteHandler();

        try {
            SAXParser file = factory.newSAXParser();
            file.parse("src/edu/xml/route2.xml", handler);
        } catch (Exception e) {
            e.printStackTrace();
        }

        handler.getMapNodeList();

        for (int i = 0; i < handler.getMapNodeList().size(); i++) {

            System.out.print(handler.getMapNodeList().get(i).getId() + " (X: " + handler.getMapNodeList().get(i).getX() + "; Y: " + handler.getMapNodeList().get(i).getY() + ") ");
            System.out.println(handler.getMapNodeList().get(i).getTags().toString());

        }

        for (int e = 0; e < handler.getMapWayList().size(); e++) {

            for (int x = 0; x < handler.getMapWayList().get(e).getNd().size(); x++) {
                System.out.print(handler.getMapWayList().get(e).getNd().get(x) + " ");
            }
            System.out.println(handler.getMapWayList().get(e).getTags().toString());
        }

    }
}
