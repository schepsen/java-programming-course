package edu;

import java.util.ArrayList;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class WeightedGraph {

    private int[][] strecke;
    private MapNode[] stadt;

    public WeightedGraph(int V) {
        strecke = new int[V][V];
        stadt = new MapNode[V];
    }

    public int getNumVertices() {
        return stadt.length;
    }

    public void setStadt(int vertex, MapNode city) {
        stadt[vertex] = city;
    }

    public MapNode getStadt(int vertex) {
        return stadt[vertex];
    }

    public void addEdge(int i, int j, int weight) {
        strecke[i][j] = weight;
        strecke[j][i] = weight;
    }

    public boolean isEdge(int i, int j) {
        return strecke[i][j] > 0;
    }

    public void removeEdge(int i, int j) {
        strecke[i][j] = 0;
        strecke[j][i] = 0;
    }

    public int getWeight(int i, int j) {
        return strecke[i][j];
    }

    public int[] neighbors(int vertex) {
        int count = 0;
        for (int i = 0; i < strecke[vertex].length; i++) {
            if (strecke[vertex][i] > 0) {
                count++;
            }
        }
        final int[] neighborslist = new int[count];
        count = 0;
        for (int i = 0; i < strecke[vertex].length; i++) {
            if (strecke[vertex][i] > 0) {
                neighborslist[count++] = i;
            }
        }
        return neighborslist;
    }

    public int getIndex(int ref) {

        for (int i = 0; i < this.getNumVertices(); i++) {
            if (this.getStadt(i).getId() == ref) {
                return i;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        SAXParserFactory factory = SAXParserFactory.newInstance();

        XMLRouteHandler handler = new XMLRouteHandler();

        try {
            SAXParser file = factory.newSAXParser();
            file.parse("src/edu/xml/route.xml", handler);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ArrayList<MapWay> WayList = handler.getMapWayList();
        ArrayList<MapNode> NodeList = handler.getMapNodeList();

        WeightedGraph G = new WeightedGraph(NodeList.size());

        for (int i = 0; i < G.getNumVertices(); i++) {
            G.setStadt(i, NodeList.get(i));
        }

        for (int i = 0; i < WayList.size(); i++) {

            for (int j = 0; j < WayList.get(i).getNd().size() - 1; j++) {

                int refS = WayList.get(i).getNd().get(j);
                int refE = WayList.get(i).getNd().get(j + 1);

                int x1 = (int) G.getStadt(G.getIndex(refS)).getX();
                int y1 = (int) G.getStadt(G.getIndex(refS)).getY();
                int x2 = (int) G.getStadt(G.getIndex(refE)).getX();
                int y2 = (int) G.getStadt(G.getIndex(refE)).getY();

                int weight = (int) Math.sqrt(Math.pow(x2 - x1, 2.0) + Math.pow(y2 - y1, 2.0));

                G.addEdge(G.getIndex(refS), G.getIndex(refE), weight);

            }

        }

        for (int step = 0; step < Dijkstra.shortestPath(G, 1, 2).size(); step++) {

            System.out.print(Dijkstra.shortestPath(G, 1, 2).get(step).getId() + " -> ");
        }
    }
}
