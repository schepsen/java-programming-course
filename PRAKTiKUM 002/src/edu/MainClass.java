package edu;

/**
 * @author Nikolaj Schepsen
 * 
 */
public class MainClass {

    public static void main(String[] args) {
        Graph DE = new Graph(16);

        DE.addEdge(0, 1);
        DE.addEdge(0, 2);
        DE.addEdge(0, 4);
        DE.addEdge(2, 4);
        DE.addEdge(2, 6);
        DE.addEdge(3, 4);
        DE.addEdge(4, 5);
        DE.addEdge(4, 6);
        DE.addEdge(4, 8);
        DE.addEdge(4, 9);
        DE.addEdge(4, 10);
        DE.addEdge(5, 6);
        DE.addEdge(5, 10);
        DE.addEdge(5, 11);
        DE.addEdge(6, 7);
        DE.addEdge(6, 11);
        DE.addEdge(8, 9);
        DE.addEdge(8, 12);
        DE.addEdge(9, 10);
        DE.addEdge(9, 12);
        DE.addEdge(9, 14);
        DE.addEdge(9, 15);
        DE.addEdge(10, 11);
        DE.addEdge(10, 15);
        DE.addEdge(11, 15);
        DE.addEdge(12, 13);
        DE.addEdge(12, 14);
        DE.addEdge(14, 15);

        // TEST: Aufgabe 1
        System.out.println("TEST: Aufgabe 1");
        System.out.println("Anzahl der Knoten: " + DE.getNumVertices());
        System.out.println("Ist (0,1) eine Kante?: " + DE.containsEdge(0,
                1));
        System.out.println("Ist (0,3) eine Kante?: " + DE.containsEdge(0,
                3));

        System.out.println("Nachbarn: " + DE.adjacentVertices(9));

        int[] Farbe = {1, 2, 2, 1, 3, 1, 4, 1, 1, 2, 4, 2, 3, 1, 1, 3};

        // TEST: Aufgabe 2a
        System.out.println("TEST: Aufgabe 2a");
        System.out.println("Ist die Färbung gültig?: " + Graph.pruefe(DE,
                Farbe));

        // TEST: Aufgabe 2b
        System.out.println("TEST: Aufgabe 2b");
        int[] Farben = new int[DE.getNumVertices()];

        // for (int i = 0; i <= DE.getNumVertices(); i++) {

        //     System.out.println("gefundene Färbung: Level " + i + " " + Graph.backtrack(DE, Farben, i));
        // }

        System.out.println("gefundene Färbung: Level 16: " + Graph.backtrack(DE, Farben, 16));
    }
}
