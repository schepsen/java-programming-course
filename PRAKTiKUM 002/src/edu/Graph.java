package edu;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author Nikolaj Schepsen
 * 
 */
public class Graph {

    Graph(int V) {
        setNumVertices(V);
    }
    private boolean[][] Bundeslaender;

    // Erstelle eine N x N Matrix
    private void setNumVertices(int N) {
        Bundeslaender = new boolean[N][N];
    }

    // Anzahl der Knoten
    int getNumVertices() {
        return Bundeslaender.length;
    }

    // Füge Kante (u,v) hinzu
    void addEdge(int u, int v) {
        Bundeslaender[u][v] = true;
        Bundeslaender[v][u] = true;
    }

    // Entferne Kante (u,v)
    void removeEdge(int u, int v) {
        Bundeslaender[u][v] = false;
        Bundeslaender[v][u] = false;
    }

    // Ist (u,v) eine Kante?
    boolean containsEdge(int u, int v) {
        return Bundeslaender[u][v];
    }

    // liefere alle angrenzenden Knoten
    ArrayList<Integer> adjacentVertices(int u) {
        ArrayList<Integer> Nachbarlaender = new ArrayList<Integer>();
        for (int v = 0; v < getNumVertices(); v++) {
            // Prüfe, ob (u,v) eine Kante bildet
            if (containsEdge(u, v)) {
                Nachbarlaender.add(v);
            }
        }
        return Nachbarlaender;
    }

    public static boolean pruefe(Graph G, int[] farben) {
        // Gehe alle Vertices durch
        for (int i = 0; i < G.getNumVertices(); i++) {
            ArrayList<Integer> adjacentVertices = G.adjacentVertices(i);
            // Prüfe, ob der Knoten gefärbt ist
            if (farben[i] != 0) {
                for (int k = 0; k < adjacentVertices.size(); k++) {
                    // Prüfe, ob die adjazenten V´s unterschiedlich gefärbt sind
                    if (farben[i] != farben[adjacentVertices.get(k)]) {
                        continue;
                    } else {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public static String backtrack(Graph G, int[] farben, int level) {
        switch (level) {
            case 0:
                return "Achtung! Der Graph ist nicht gefärbt.";
            default:
                // "Knoten"-Schleife
                for (int i = 0; i < farben.length; i++) {
                    if (farben[i] == 0) {
                        // "Farb"-Schleife
                        for (int c = 1; c <= 4; c++) {
                            // Setze die Farbe des Knotens
                            farben[i] = c;
                            // Prüfe, ob die Färbung dem Knoten passt
                            if (pruefe(G, farben) && i < level - 1) {
                                // Wenns passt, prüfe den nächsten Knoten
                                break; // raus aus der "Farb"-Schleife
                            } else if (pruefe(G, farben)) {
                                // Lösungsausgabe, falls sie gefunden wurde
                                return Arrays.toString(farben);
                            } else {
                                continue;
                            }
                        }
                        //return "1not found";
                    }
                }
                break;
        }
        return "NOT FOUND";
        // return null;

    }
}
