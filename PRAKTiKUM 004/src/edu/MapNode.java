package edu;

import java.util.Hashtable;

public class MapNode {

    private int id;
    private float x;
    private float y;

    private Hashtable<String, String> tags = new Hashtable<String, String>();

    MapNode(int id, float x, float y) {
        this.setId(id);
        this.setX(x);
        this.setY(y);

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }
    
    public void addTag(String key, String value){
        this.tags.put(key, value);
    }

    public Hashtable<String, String> getTags() {
        return this.tags;
    }

    public void setTags(Hashtable<String, String> ht) {
        this.tags = ht;
    }

}
