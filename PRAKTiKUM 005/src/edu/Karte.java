package edu;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.swing.JComponent;

class Kreis {

    double x, y, r;
    boolean isFilled;

    public Kreis(double x, double y, double radius, boolean isFilled) {
        this.x = x;
        this.y = y;
        this.r = radius;
        this.isFilled = isFilled;
    }
}

class Karte extends JComponent {

    private List<Kreis> circles;
    private double tx, ty, scale;
    private double modX = -1;
    private double modY = -1;
    private int modIndex = -1;
    public double winkel;

    public List<Kreis> getList() {
        return circles;
    }

    public void setTX(double x) {
        tx = x;
    }

    public double getTX() {
        return tx;
    }

    public double getTY() {
        return ty;
    }

    public void setTY(double y) {
        ty = y;
    }

    public void setScale(double factor) {
        scale = factor;
    }

    public double getModX() {
        return modX;
    }

    public double getModY() {
        return modY;
    }

    public double getScale() {
        return scale;
    }

    public Karte() {
        DragZoom dragZoom = new DragZoom();
        addMouseListener(dragZoom);
        addMouseMotionListener(dragZoom);
        addMouseWheelListener(dragZoom);

        circles = new ArrayList<Kreis>();
        tx = 0;
        ty = 0;

        winkel = 0;
        scale = 0.1;

        for (int i = 1; i <= 500; i++) {
            Random r = new Random();
            Kreis c = new Kreis(r.nextInt(4000), r.nextInt(4000), 2, false);
            circles.add(c);
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, getWidth(), getHeight());
        Graphics2D g2d = (Graphics2D) g;
        g2d.setStroke(new BasicStroke(1.2f));
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);

        g2d.transform(AffineTransform.getRotateInstance(winkel, this.getWidth() / 2, this.getHeight() / 2));

        for (Kreis c : circles) {
            double x = c.x * scale + tx;
            double y = c.y * scale + ty;
            double r = c.r * scale;

            Ellipse2D obj = new Ellipse2D.Double(x - r, y - r, 2 * r, 2 * r);

            g2d.setColor(Color.BLACK);

            if (c.isFilled) {
                g2d.setColor(Color.RED);
                g2d.fill(obj);
            }

            g2d.draw(obj);
        }
    }

    private class DragZoom extends MouseAdapter {

        private static final int button = MouseEvent.BUTTON3;
        private boolean isDragging;
        private Point previous;
        boolean isModded = false;

        @Override
        public void mousePressed(MouseEvent e) {
            if (!isDragging && e.getButton() == button) {
                isDragging = true;
                previous = new Point(e.getPoint());
            }

            if (e.getButton() == MouseEvent.BUTTON1) {

                double mx = (e.getX() - tx) / scale;
                double my = (e.getY() - ty) / scale;

                boolean isFound = false;

                int distanz = 10;

                while (!isFound) {
                    for (int index = 0; index < circles.size(); index++) {
                        double cx = circles.get(index).x;
                        double cy = circles.get(index).y;
                        double entfernung = Math.sqrt(Math.pow(cx - mx, 2.0) + Math.pow(cy - my, 2.0));

                        if (entfernung <= distanz) {

                            if (isModded) {
                                circles.set(modIndex, new Kreis(modX, modY, 2, false));
                            }

                            circles.set(index, new Kreis(cx, cy, 4, true));

                            modX = cx;
                            modY = cy;
                            modIndex = index;

                            isModded = true;
                            isFound = true;

                            repaint();
                            break;
                        }
                    }
                    distanz += 10;
                }
            }
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            if (isDragging && e.getButton() == button) {
                isDragging = false;
            }
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            if (isDragging) {
                Point point = e.getPoint();
                tx += point.x - previous.x;
                ty += point.y - previous.y;
                previous = new Point(point);
                repaint();
            }
        }

        @Override
        public void mouseWheelMoved(MouseWheelEvent e) {
            double factor = Math.pow(1.1, e.getWheelRotation());
            Point point = e.getPoint();
            tx += (1 - factor) * (point.x - tx);
            ty += (1 - factor) * (point.y - ty);
            scale *= factor;
            repaint();
        }
    }
}
