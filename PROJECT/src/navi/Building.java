package navi;
/**
 * @author Thi H. 
 * @author Enis 
 * @author Nikolaj S.
 * 
 *  Gruppe 22 (PRG-PR-2011)
 */
import java.awt.Polygon;
import java.util.Hashtable;

public class Building {

    private Polygon umriss;
    private Hashtable<String, String> tags;

    public Building(Polygon umriss, Hashtable<String, String> tags) {
        this.umriss = umriss;
        this.tags = tags;
    }

    public Polygon getUmriss() {
        return umriss;
    }

    public void setUmriss(Polygon umriss) {
        this.umriss = umriss;
    }

    public Hashtable<String, String> getTags() {
        return tags;
    }

    public void setTags(Hashtable<String, String> tags) {
        this.tags = tags;
    }
}