package navi;
/**
 * @author Thi H. 
 * @author Enis 
 * @author Nikolaj S.
 * 
 *  Gruppe 22 (PRG-PR-2011)
 */
import java.util.ArrayList;
import java.util.Hashtable;

public class Kanten {

    private ArrayList<Knoten> nd;
    private Hashtable<String, String> tags = new Hashtable<String, String>();

    public Kanten(ArrayList<Knoten> nd) {
        this.setNd(nd);
    }

    public void addNd(Knoten ref) {
        this.nd.add(ref);
    }

    public ArrayList<Knoten> getNd() {
        return this.nd;
    }

    public void setNd(ArrayList<Knoten> nd) {
        this.nd = nd;
    }

    public void addTags(String key, String value) {
        this.tags.put(key, value);
    }

    public Hashtable<String, String> getTags() {
        return this.tags;
    }

    public void setTags(Hashtable<String, String> tags) {
        this.tags = tags;
    }

}