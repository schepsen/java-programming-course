package navi.gui;
/**
 * @author Thi H. 
 * @author Enis 
 * @author Nikolaj S.
 * 
 *  Gruppe 22 (PRG-PR-2011)
 */
import java.io.File;

import javax.swing.filechooser.FileFilter;

class OSMFilter extends FileFilter {

    String endung = ".osm";

    @Override
    public boolean accept(File path) {
        return path.isDirectory() || path.getAbsolutePath().endsWith(endung);
    }

    @Override
    public String getDescription() {
        return "OpenStreetMap XML File (" + endung + ")";
    }

}