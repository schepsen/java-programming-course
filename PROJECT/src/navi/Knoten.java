package navi;
/**
 * @author Thi H. 
 * @author Enis 
 * @author Nikolaj S.
 * 
 *  Gruppe 22 (PRG-PR-2011)
 */
import java.util.Hashtable;
import java.awt.geom.Point2D;

public class Knoten {

    private int id;
    private Point2D loc;
    private String type;
    private boolean isSelected;
    private Hashtable<String, String> tags = new Hashtable<String, String>();

    public Knoten(int id, Point2D location) {
        this.setId(id);
        this.setLocation(location);
        this.isSelected(false);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Point2D getLocation() {
        return loc;
    }

    public void setLocation(Point2D location) {
        this.loc = location;
    }

    public void addTags(String key, String value) {
        this.tags.put(key, value);
    }

    public Hashtable<String, String> getTags() {
        return tags;
    }

    public void setTags(Hashtable<String, String> tags) {
        this.tags = tags;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void isSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    @Override
    public String toString() {

        String message = (getTags()!=null && getTags().containsKey("addr:street")) ? getTags().get("addr:street") + " " + getTags().get("addr:housenumber") : "keine Adresse vorhanden";
        return message;

    }
}