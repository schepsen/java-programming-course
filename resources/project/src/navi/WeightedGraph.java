package navi;
/**
 * @author Thi H. 
 * @author Enis 
 * @author Nikolaj S.
 * 
 *  Gruppe 22 (PRG-PR-2011)
 */
public final class WeightedGraph {

    private GEdge[][] edges;
    private Knoten[] vertices;

    public WeightedGraph(int V) {
        setEdges(new GEdge[V][V]);
        setVertices(new Knoten[V]);
    }

    public void addEdge(int i, int j, String name, int wigth, boolean oneway, String type, int maxspeed) {

        GEdge kante = new GEdge(name, wigth, type, maxspeed);

        if (oneway) {
            edges[i][j] = kante;
        } else {
            edges[i][j] = kante;
            edges[j][i] = kante;
        }
    }

    public GEdge[][] getEdges() {
        return edges;
    }

    public void setEdges(GEdge[][] Edges) {
        this.edges = Edges;
    }

    public void addVertex(int i, Knoten v) {
        vertices[i] = v;
    }

    public Knoten getVertex(int i) {
        return vertices[i];
    }

    public void setVertices(Knoten[] Vertices) {
        this.vertices = Vertices;
    }

    public int getNumVertices() {
        return vertices.length;
    }

    public boolean isEdge(int i, int j) {
        return edges[i][j] != null;
    }

    public void removeEdge(int i, int j) {
        edges[i][j] = null;
        edges[j][i] = null;
    }

    public int getWeight(int i, int j) {
        return edges[i][j].getWight();
    }

    public int getSpeed(int i, int j) {
        return edges[i][j].getMaxSpeed();
    }

    public int getTime(int i, int j) {
        int strafe = 0;

        if (this.getVertex(j).getTags() != null) {
            if (this.getVertex(j).getTags().containsKey("highway")) {
                /** Strafe #1 (Ampel) */
                strafe = (this.getVertex(j).getTags().get("highway").equals("traffic_signals")) ? 10 : 0;
            }
        }
        /**
         * Es wird die Zeit (in Sekunden), die für diese Strecke zurückgeliefert
         */
        return (this.getWeight(i, j) / (this.getSpeed(i, j) * 1000 / 3600)) + strafe;
    }

    public String getStreet(int i, int j) {
        return (edges[i][j].getName() != null) ? edges[i][j].getName() : "unbekannte Straße";
    }

    public int[] neighbors(int vertex) {
        int count = 0;
        for (int i = 0; i < edges[vertex].length; i++) {
            if (edges[vertex][i] != null) {
                count++;
            }
        }
        final int[] neighborslist = new int[count];
        count = 0;
        for (int i = 0; i < edges[vertex].length; i++) {
            if (edges[vertex][i] != null) {
                neighborslist[count++] = i;
            }
        }
        return neighborslist;
    }

    public int[] neighborsH(int vertex) {
        int count = 0;
        for (int i = 0; i < edges[vertex].length; i++) {
            if (edges[vertex][i] != null && edges[vertex][i].getType().equals("highway")) {
                count++;
            }
        }
        final int[] neighborslist = new int[count];
        count = 0;
        for (int i = 0; i < edges[vertex].length; i++) {
            if (edges[vertex][i] != null && edges[vertex][i].getType().equals("highway")) {
                neighborslist[count++] = i;
            }
        }
        return neighborslist;
    }

    public int getIndex(int ref) {

        for (int i = 0; i < this.getNumVertices(); i++) {
            if (getVertex(i).getId() == ref) {
                return i;
            }
        }
        return -1;
    }
}
