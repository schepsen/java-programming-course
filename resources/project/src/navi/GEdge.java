package navi;
/**
 * @author Thi H. 
 * @author Enis 
 * @author Nikolaj S.
 * 
 *  Gruppe 22 (PRG-PR-2011)
 */
public class GEdge {

    private String name;
    private int wight;
    private boolean oneway;
    private String type;
    private int maxspeed;

    public GEdge() {

    }

    public GEdge(String name, int wigth, String type, int maxspeed) {
        setName(name);
        setWight(wigth);
        setType(type);
        setMaxSpeed(maxspeed);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWight() {
        return wight;
    }

    public void setWight(int wight) {
        this.wight = wight;
    }

    public boolean isOneway() {
        return oneway;
    }

    public void setOneway(boolean oneway) {
        this.oneway = oneway;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getMaxSpeed() {
        return maxspeed;
    }

    public void setMaxSpeed(int maxspeed) {
        this.maxspeed = maxspeed;
    }

}
