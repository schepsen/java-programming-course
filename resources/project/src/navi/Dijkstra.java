package navi;
/**
 * @author Thi H. 
 * @author Enis 
 * @author Nikolaj S.
 * 
 *  Gruppe 22 (PRG-PR-2011)
 */
import java.util.ArrayList;

public class Dijkstra {

    public static int[] dijkstraS(WeightedGraph G, int s, String type) {
        final int[] dist = new int[G.getNumVertices()];
        final int[] pred = new int[G.getNumVertices()];
        final boolean[] visited = new boolean[G.getNumVertices()];

        for (int i = 0; i < dist.length; i++) {
            dist[i] = Integer.MAX_VALUE;
        }
        dist[s] = 0;

        for (int i = 0; i < dist.length; i++) {
            final int next = minVertex(dist, visited);
            visited[next] = true;

            final int[] n = (type.equals("fw")) ? G.neighbors(next) : G.neighborsH(next);
            for (int j = 0; j < n.length; j++) {
                final int v = n[j];
                final int d = dist[next] + G.getWeight(next, v);
                if (dist[v] > d) {
                    dist[v] = d;
                    pred[v] = next;
                }
            }
        }
        return pred;
    }

    public static int[] dijkstraF(WeightedGraph G, int s, String type) {
        final int[] dist = new int[G.getNumVertices()];
        final int[] pred = new int[G.getNumVertices()];
        final boolean[] visited = new boolean[G.getNumVertices()];

        for (int i = 0; i < dist.length; i++) {
            dist[i] = Integer.MAX_VALUE;
        }
        dist[s] = 0;

        for (int i = 0; i < dist.length; i++) {
            final int next = minVertex(dist, visited);
            visited[next] = true;

            final int[] n = (type.equals("fw")) ? G.neighbors(next) : G.neighborsH(next);
            for (int j = 0; j < n.length; j++) {
                final int v = n[j];
                final int d = dist[next] + G.getTime(next, v);
                if (dist[v] > d) {
                    dist[v] = d;
                    pred[v] = next;
                }
            }
        }
        return pred;
    }

    private static int minVertex(int[] dist, boolean[] v) {
        int x = Integer.MAX_VALUE;
        int y = 0;
        for (int i = 0; i < dist.length; i++) {
            if (!v[i] && dist[i] < x) {
                y = i;
                x = dist[i];
            }
        }
        return y;
    }

    // @SuppressWarnings({ "rawtypes", "unchecked" })
    public static ArrayList<Knoten> shortestPath(WeightedGraph G, int start, int end, int[] pred) {

        ArrayList<Knoten> path = new ArrayList<Knoten>();
        int x = end;
        while (x != start) {
            path.add(0, G.getVertex(x));
            x = pred[x];
        }
        path.add(0, G.getVertex(start));

        /** Testen, ob dieser Pfad ein gültiger Pfad ist */
        for (int el = 0; el < path.size() - 1; el++) {
            if (!G.isEdge(G.getIndex(path.get(el).getId()), G.getIndex(path.get(el + 1).getId()))) {
                return null;
            }
        }
        return path;
    }
}