package navi.osm;

import java.awt.geom.Point2D;
import java.util.ArrayList;

import navi.Kanten;
import navi.Knoten;
import navi.osm.Coordinates.UTMPoint;
import navi.osm.Coordinates.WGS84Point;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class OSMHandler extends DefaultHandler {

    long zstVorher;
    long zstNachher;

    private Knoten nd_obj;
    private Kanten way_obj;
    private boolean bNode = false;
    private boolean bWays = false;
    private UTMPoint Bounds;
    private ArrayList<Knoten> refList;
    /* KNOTEN & KANTEN* */
    private ArrayList<Kanten> edges = new ArrayList<Kanten>();
    private ArrayList<Knoten> vertices = new ArrayList<Knoten>();

    public ArrayList<Kanten> getWayList() {
        return edges;
    }

    public ArrayList<Knoten> getNodeList() {
        return vertices;
    }

    @Override
    public void startDocument() throws SAXException {
        zstVorher = System.currentTimeMillis();
    }

    @Override
    public void endDocument() throws SAXException {
        System.out.println("Die Datei wurde in: " + ((System.currentTimeMillis() - zstVorher) / 1000) + "  Sekunden geparst");
    }

    @Override
    public void startElement(String namespaceURI, String localname, String tagname, Attributes attributes) throws SAXException {

        if (Bounds == null)
            Bounds = new UTMPoint(0, 0, 0);

        if (tagname.equals("node")) {

            bNode = true;

            int id = Integer.parseInt(attributes.getValue("id"));
            UTMPoint GPSPoint = convert(Double.parseDouble(attributes.getValue("lat")), Double.parseDouble(attributes.getValue("lon")));
            nd_obj = new Knoten(id, new Point2D.Double(GPSPoint.getX() - Bounds.getX(), -1 * (GPSPoint.getY() - Bounds.getY())));
            vertices.add(nd_obj);

        } else if (tagname.equals("tag")) {

            if (bNode) {
                String key = attributes.getValue(0);
                String value = attributes.getValue(1);

                if (key.equals("addr:street")) {
                    vertices.get(vertices.size() - 1).setType("address");
                }

                vertices.get(vertices.size() - 1).addTags(key, value);

            } else if (bWays) {
                String key = attributes.getValue(0);
                String value = attributes.getValue(1);

                edges.get(edges.size() - 1).addTags(key, value);
            }
        }

        else if (tagname.equals("way")) {

            bWays = true;
            refList = new ArrayList<Knoten>();
            way_obj = new Kanten(refList);
            edges.add(way_obj);

        }

        else if (tagname.equals("nd")) {

            int ref = Integer.parseInt(attributes.getValue(0));

            for (int i = 0; i < vertices.size(); i++) {
                if (vertices.get(i).getId() == ref) {
                    edges.get(edges.size() - 1).addNd(vertices.get(i));
                    break;
                }
            }

        } else if (tagname.equals("bounds")) {
            Bounds = convert(Double.parseDouble(attributes.getValue("minlat")), Double.parseDouble(attributes.getValue("minlon")));
        }
    }

    private UTMPoint convert(double latitude, double longitude) {
        WGS84Point wgs = new WGS84Point();
        wgs.SetDegrees(latitude, longitude);
        UTMPoint GPSPoint = new UTMPoint();
        Coordinates.convertWGS84toUTM(wgs, GPSPoint);

        return GPSPoint;
    }

    @Override
    public void endElement(String namespaceURI, String localname, String tagname) {
        if (tagname.equals("node")) {
            bNode = false;
        }

        if (tagname.equals("way")) {
            bWays = false;
        }
    }
}
