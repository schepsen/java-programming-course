package navi.gui;

/**
 * @author Thi H. 
 * @author Enis 
 * @author Nikolaj S.
 * 
 *  Gruppe 22 (PRG-PR-2011)
 */
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import navi.osm.OSMHandler;

import javax.swing.JComponent;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import navi.Building;
import navi.GEdge;
import navi.Kanten;
import navi.Knoten;
import navi.WeightedGraph;

public class Karte extends JComponent {

    private static final long serialVersionUID = 1L;
    private double tx, ty, scale;
    private Point previous;
    private boolean isDragged;
    ArrayList<Knoten> MapNode;
    public ArrayList<Knoten> MapNodes;

    Knoten SP;
    Knoten ZP;

    Point2D P1;
    Point2D P2;

    double jFrameWinkel = 0.0;

    Point2D LP1;
    Point2D LP2;

    int rIndex;
    
    ArrayList<String> strassen;
    ArrayList<Point2D> kreuzungen;

    ArrayList<Knoten> shortestpath;
    WeightedGraph G;
    ArrayList<Kanten> mapways;
    ArrayList<String> fways = new ArrayList<String>();
    ArrayList<String> mways = new ArrayList<String>();
    /* CSS STYLE 1 * */
    Color background = new Color(0xF4F3EF);
    Color building = new Color(0xDFDAD4);
    Color edge = new Color(0xBAA5A4);
    Color footway = new Color(0xC6C6C6);
    Color riverborder = new Color(0xA6BFDD);
    Color road = new Color(0xFCC44B);
    Color waterway = new Color(0xA6AFDD);

    BasicStroke bsRand = new BasicStroke(10.0f);
    BasicStroke bsStreet = new BasicStroke(7.4f);

    BasicStroke bsFRand = new BasicStroke(7.0f);
    BasicStroke bsFStreet = new BasicStroke(5.0f);
    BasicStroke bsDefault = new BasicStroke(2.3f);

    /* BUILDINGS & RIVERBANKS * */
    public ArrayList<Building> buildings;
    public ArrayList<Polygon> riverbanks;
    /* HIGHWAYS & WATERWAYS * */
    public ArrayList<Line2D> highways;
    public ArrayList<Line2D> footways;
    public ArrayList<Line2D> waterways;
    public ArrayList<GEdge> gedges;
    private Image img1 = Toolkit.getDefaultToolkit().getImage("src/navi/gui/images/marker.png");

    public Karte() {

        setTX(0.0);
        setTY(0.0);

        setScale(1.0);

        fways.add("footway");
        fways.add("living_street");
        fways.add("path");
        fways.add("pedestrian");
        fways.add("service");
        fways.add("steps");

        mways.add("motorway");
        mways.add("primary");
        mways.add("residential");
        mways.add("road");
        mways.add("secondary");
        mways.add("tertiary");
        mways.add("track");
        mways.add("trunk");
        mways.add("unclassified");

    }

    public double getTX() {
        return tx;
    }

    public void setTX(double x) {
        this.tx = x;
    }

    public double getTY() {
        return ty;
    }

    public void setTY(double y) {
        this.ty = y;
    }

    public double getScale() {
        return scale;
    }

    public void setScale(double scale) {
        this.scale = scale;
    }

    public Point getPreviousPoint() {
        return previous;
    }

    public void setPreviousPoint(Point previous) {
        this.previous = previous;
    }

    public boolean isDragged() {
        return isDragged;
    }

    public void setDragging(boolean isDragged) {
        this.isDragged = isDragged;
    }

    /* readOSMKarte() * */
    public void readOSMKarte(String path) {

        shortestpath = null;

        MapNode = new ArrayList<Knoten>();
        MapNodes = new ArrayList<Knoten>();
        /* BUILDINGS & RIVERBANKS * */
        buildings = new ArrayList<Building>();
        riverbanks = new ArrayList<Polygon>();
        /* HIGHWAYS & WATERWAYS * */
        highways = new ArrayList<Line2D>();
        footways = new ArrayList<Line2D>();
        waterways = new ArrayList<Line2D>();
        gedges = new ArrayList<GEdge>();

        SAXParserFactory factory = SAXParserFactory.newInstance();

        OSMHandler handler = new OSMHandler();

        try {

            SAXParser file = factory.newSAXParser();
            file.parse(path, handler);

        } catch (Exception e) {

            e.printStackTrace();

        }

        mapways = handler.getWayList();
        MapNodes = handler.getNodeList();

        for (int i = 0; i < mapways.size(); i++) {

            if (mapways.get(i).getTags().containsKey("building")) {
                /* searching for buildings * */

                int nds = mapways.get(i).getNd().size();

                int[] x = new int[nds];
                int[] y = new int[nds];

                for (int b = 0; b < nds; b++) {

                    x[b] = (int) mapways.get(i).getNd().get(b).getLocation().getX();
                    y[b] = (int) mapways.get(i).getNd().get(b).getLocation().getY();

                }

                buildings.add(new Building(new Polygon(x, y, nds), mapways.get(i).getTags()));

            } else if (mapways.get(i).getTags().containsKey("waterway")) {
                /* searching for waterways(riverbanks) * */

                if (mapways.get(i).getTags().get("waterway").equals("riverbank")) {

                    int nds = mapways.get(i).getNd().size();

                    int[] x = new int[nds];
                    int[] y = new int[nds];

                    for (int b = 0; b < nds; b++) {

                        x[b] = (int) mapways.get(i).getNd().get(b).getLocation().getX();
                        y[b] = (int) mapways.get(i).getNd().get(b).getLocation().getY();

                    }

                    riverbanks.add(new Polygon(x, y, nds));

                } else {

                    for (int j = 0; j < mapways.get(i).getNd().size() - 1; j++) {

                        Point2D P1 = mapways.get(i).getNd().get(j).getLocation();
                        Point2D P2 = mapways.get(i).getNd().get(j + 1).getLocation();

                        waterways.add(new Line2D.Double(P1, P2));

                    }
                }

            } else if (mapways.get(i).getTags().containsKey("highway")) {

                if (fways.contains(mapways.get(i).getTags().get("highway"))) {

                    for (int j = 0; j < mapways.get(i).getNd().size() - 1; j++) {

                        if (!MapNode.contains(mapways.get(i).getNd().get(j))) {
                            mapways.get(i).getNd().get(j).setType("footway");
                            MapNode.add(mapways.get(i).getNd().get(j));
                        }

                        Point2D P1 = mapways.get(i).getNd().get(j).getLocation();
                        Point2D P2 = mapways.get(i).getNd().get(j + 1).getLocation();

                        footways.add(new Line2D.Double(P1, P2));

                    }

                    if (!MapNode.contains(mapways.get(i).getNd().get(mapways.get(i).getNd().size() - 1))) {
                        mapways.get(i).getNd().get(mapways.get(i).getNd().size() - 1).setType("footway");
                        MapNode.add(mapways.get(i).getNd().get(mapways.get(i).getNd().size() - 1));
                    }

                } else if (mways.contains(mapways.get(i).getTags().get("highway"))) {

                    for (int j = 0; j < mapways.get(i).getNd().size() - 1; j++) {

                        if (!MapNode.contains(mapways.get(i).getNd().get(j))) {
                            mapways.get(i).getNd().get(j).setType("highway");
                            MapNode.add(mapways.get(i).getNd().get(j));
                        } else {
                            MapNode.get(MapNode.indexOf(mapways.get(i).getNd().get(j))).setType("highway");
                        }

                        Point2D P1 = mapways.get(i).getNd().get(j).getLocation();
                        Point2D P2 = mapways.get(i).getNd().get(j + 1).getLocation();

                        highways.add(new Line2D.Double(P1, P2));

                    }

                    if (!MapNode.contains(mapways.get(i).getNd().get(mapways.get(i).getNd().size() - 1))) {
                        mapways.get(i).getNd().get(mapways.get(i).getNd().size() - 1).setType("highway");
                        MapNode.add(mapways.get(i).getNd().get(mapways.get(i).getNd().size() - 1));
                    }

                }

            }

        }
        createGraph(true);
    }

    void createGraph(boolean onlyHighWays) {

        if (G != null) {
            G = null;
        }

        // System.out.println("1 okey");
        G = new WeightedGraph(MapNode.size());
        // System.out.println("2 okey");

        for (int index = 0; index < MapNode.size(); index++) {
            G.addVertex(index, MapNode.get(index));
        }

        // System.out.println("3 okey");
        if (onlyHighWays) {

            for (int i = 0; i < mapways.size(); i++) {

                if (mapways.get(i).getTags().containsKey("highway")) {

                    if (fways.contains(mapways.get(i).getTags().get("highway"))) {

                        for (int j = 0; j < mapways.get(i).getNd().size() - 1; j++) {

                            int maxspeed = Integer.parseInt((mapways.get(i).getTags().get("maxspeed") != null) ? mapways.get(i).getTags().get("maxspeed") : "50");
                            boolean oneway = (mapways.get(i).getTags().get("oneway") != null) ? true : false;

                            int wigth = (int) mapways.get(i).getNd().get(j).getLocation().distance(mapways.get(i).getNd().get(j + 1).getLocation());

                            G.addEdge(MapNode.indexOf(mapways.get(i).getNd().get(j)), MapNode.indexOf(mapways.get(i).getNd().get(j + 1)), mapways.get(i).getTags().get("name"),
                                    wigth, oneway, "footway", maxspeed);

                        }

                    } else if (mways.contains(mapways.get(i).getTags().get("highway"))) {

                        for (int j = 0; j < mapways.get(i).getNd().size() - 1; j++) {

                            int maxspeed = Integer.parseInt((mapways.get(i).getTags().get("maxspeed") != null) ? mapways.get(i).getTags().get("maxspeed") : "50");
                            boolean oneway = (mapways.get(i).getTags().get("oneway") != null) ? true : false;

                            int wigth = (int) mapways.get(i).getNd().get(j).getLocation().distance(mapways.get(i).getNd().get(j + 1).getLocation());

                            G.addEdge(MapNode.indexOf(mapways.get(i).getNd().get(j)), MapNode.indexOf(mapways.get(i).getNd().get(j + 1)), mapways.get(i).getTags().get("name"),
                                    wigth, oneway, "highway", maxspeed);

                        }

                    }

                }
            }
        } else {

            for (int i = 0; i < mapways.size(); i++) {

                if (mapways.get(i).getTags().containsKey("highway")) {

                    if (fways.contains(mapways.get(i).getTags().get("highway"))) {

                        for (int j = 0; j < mapways.get(i).getNd().size() - 1; j++) {

                            int maxspeed = Integer.parseInt((mapways.get(i).getTags().get("maxspeed") != null) ? mapways.get(i).getTags().get("maxspeed") : "50");

                            int wigth = (int) mapways.get(i).getNd().get(j).getLocation().distance(mapways.get(i).getNd().get(j + 1).getLocation());

                            G.addEdge(MapNode.indexOf(mapways.get(i).getNd().get(j)), MapNode.indexOf(mapways.get(i).getNd().get(j + 1)), mapways.get(i).getTags().get("name"),
                                    wigth, false, "footway", maxspeed);

                        }

                    } else if (mways.contains(mapways.get(i).getTags().get("highway"))) {

                        for (int j = 0; j < mapways.get(i).getNd().size() - 1; j++) {

                            int maxspeed = Integer.parseInt((mapways.get(i).getTags().get("maxspeed") != null) ? mapways.get(i).getTags().get("maxspeed") : "50");

                            int wigth = (int) mapways.get(i).getNd().get(j).getLocation().distance(mapways.get(i).getNd().get(j + 1).getLocation());

                            G.addEdge(MapNode.indexOf(mapways.get(i).getNd().get(j)), MapNode.indexOf(mapways.get(i).getNd().get(j + 1)), mapways.get(i).getTags().get("name"),
                                    wigth, false, "highway", maxspeed);

                        }

                    }

                }
            }

        }
    }

    public double getAngle(Point2D P1, Point2D P2) {

        double angle = Math.toDegrees(Math.atan2(P1.getX() - P2.getX(), P1.getY() - P2.getY()));
        angle = angle - 180;
        if (angle > 360) {
            angle = angle - 360;
        } else if (angle < 0) {
            angle = angle + 360;
        }
        return angle;
    }

    @Override
    protected void paintComponent(Graphics g) {

        g.setColor(background);
        g.fillRect(0, 0, getWidth(), getHeight());

        Graphics2D g2 = (Graphics2D) g;

        g2.setStroke(new BasicStroke(2.5f));
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);

        if (LP1 != null) {
            ((Graphics2D) g).transform(AffineTransform.getRotateInstance((180 + jFrameWinkel) * Math.PI / 180.0, getWidth() / 2, getHeight() / 2));
        }
        if (MapNode != null && MapNodes != null && buildings != null && riverbanks != null && highways != null && footways != null && waterways != null && gedges != null) {

            /** POLYGONS -> RIVERBANKS */
            for (Polygon r : riverbanks) {
                g2.setColor(waterway);
                Polygon wcopy = new Polygon(r.xpoints, r.ypoints, r.npoints);

                for (int index = 0; index < wcopy.npoints; index++) {

                    wcopy.xpoints[index] = (int) (r.xpoints[index] * scale + tx);
                    wcopy.ypoints[index] = (int) (r.ypoints[index] * scale + ty);
                }
                g2.drawPolygon(wcopy);
                g2.setColor(riverborder);
                g2.fillPolygon(wcopy);
            }

            Graphics2D g3 = (Graphics2D) g;

            /** LINE2D -> (I) WATERWAYS */
            g3.setColor(riverborder);
            for (Line2D river : waterways) {

                double x1 = river.getX1() * scale + tx;
                double y1 = river.getY1() * scale + ty;
                double x2 = river.getX2() * scale + tx;
                double y2 = river.getY2() * scale + ty;

                g3.draw(new Line2D.Double(x1, y1, x2, y2));
            }

            /** LINE2D -> (II) FOOTWAYS -> RAND */
            g3.setColor(footway);
            g3.setStroke(bsFRand);
            for (Line2D gehweg : footways) {

                double x1 = gehweg.getX1() * scale + tx;
                double y1 = gehweg.getY1() * scale + ty;
                double x2 = gehweg.getX2() * scale + tx;
                double y2 = gehweg.getY2() * scale + ty;

                g3.draw(new Line2D.Double(x1, y1, x2, y2));
            }

            /** LINE2D -> (II) FOOTWAYS -> FILL */
            g3.setStroke(bsFStreet);
            g3.setColor(Color.WHITE);
            for (Line2D gehweg : footways) {

                double x1 = gehweg.getX1() * scale + tx;
                double y1 = gehweg.getY1() * scale + ty;
                double x2 = gehweg.getX2() * scale + tx;
                double y2 = gehweg.getY2() * scale + ty;

                g3.draw(new Line2D.Double(x1, y1, x2, y2));
            }

            /** LINE2D -> (III) HIGHWAYS -> RAND */
            g3.setStroke(bsRand);
            g3.setColor(road);
            for (Line2D straße : highways) {

                double x1 = straße.getX1() * scale + tx;
                double y1 = straße.getY1() * scale + ty;
                double x2 = straße.getX2() * scale + tx;
                double y2 = straße.getY2() * scale + ty;

                g3.draw(new Line2D.Double(x1, y1, x2, y2));
            }

            /** LINE2D -> (III) HIGHWAYS -> FILL */
            g3.setStroke(bsStreet);
            g3.setColor(Color.WHITE);
            for (Line2D straße : highways) {

                double x1 = straße.getX1() * scale + tx;
                double y1 = straße.getY1() * scale + ty;
                double x2 = straße.getX2() * scale + tx;
                double y2 = straße.getY2() * scale + ty;

                g3.draw(new Line2D.Double(x1, y1, x2, y2));
            }

            /** POLYGONS -> BUILDINGS */
            g2.setStroke(bsDefault);
            for (Building b : buildings) {
                g2.setColor(edge);
                Polygon pcopy = new Polygon(b.getUmriss().xpoints, b.getUmriss().ypoints, b.getUmriss().npoints);

                for (int index = 0; index < pcopy.npoints; index++) {

                    pcopy.xpoints[index] = (int) (b.getUmriss().xpoints[index] * scale + tx);
                    pcopy.ypoints[index] = (int) (b.getUmriss().ypoints[index] * scale + ty);
                }

                g2.drawPolygon(pcopy);
                g2.setColor(building);
                g2.fillPolygon(pcopy);
            }

            Graphics2D g4 = (Graphics2D) g;
            /** STRAßENPUNKTE */
            g4.setColor(Color.BLACK);

            for (Knoten kw : MapNode) {
                g4.setStroke(new BasicStroke(0.8f));
                //
                double x = kw.getLocation().getX() * scale + tx;
                double y = kw.getLocation().getY() * scale + ty;
                //
                g4.draw(new Ellipse2D.Double(x - 1, y - 1, 2, 2));
            }

            /** ADRESSEN */
            for (Knoten km : MapNodes) {

                if (km.getType() != null && km.getType().equals("address")) {
                    g4.setColor(Color.BLACK);
                    g4.setStroke(new BasicStroke(1.2f));
                    double x = km.getLocation().getX() * scale + tx;
                    double y = km.getLocation().getY() * scale + ty;

                    if (km.isSelected()) {
                        g4.fill(new Rectangle2D.Double(x - 4, y - 4, 8, 8));
                        g4.setColor(Color.RED);
                        g4.fill(new Rectangle2D.Double(x - 3, y - 3, 6, 6));
                    }

                    g4.draw(new Rectangle2D.Double(x - 2, y - 2, 4, 4));
                }

            }

            /** KÜRZESTER WEG */
            if (shortestpath != null) {
                g3.setColor(road);
                g3.setStroke(new BasicStroke(5.0f));

                for (int i = 0; i < shortestpath.size() - 1; i++) {

                    double x1 = shortestpath.get(i).getLocation().getX() * scale + tx;
                    double y1 = shortestpath.get(i).getLocation().getY() * scale + ty;
                    double x2 = shortestpath.get(i + 1).getLocation().getX() * scale + tx;
                    double y2 = shortestpath.get(i + 1).getLocation().getY() * scale + ty;

                    g3.draw(new Line2D.Double(x1, y1, x2, y2));
                }
            }

            /** STARTPUNKT */
            if (SP != null) {

                double x = SP.getLocation().getX() * scale + tx;
                double y = SP.getLocation().getY() * scale + ty;

                g4.drawImage(img1, (int) x - 12, (int) y - 27, this);

                g4.setColor(Color.BLACK);
                g4.drawString("START", (int) x - 15, (int) y - 28);
                g4.fill(new Ellipse2D.Double(x - 4, y - 4, 8, 8));
                g4.setColor(Color.RED);
                g4.fill(new Ellipse2D.Double(x - 3, y - 3, 6, 6));
            }

            /** ZIELPUNKT */
            if (ZP != null) {
                double x = ZP.getLocation().getX() * scale + tx;
                double y = ZP.getLocation().getY() * scale + ty;

                g4.drawImage(img1, (int) x - 12, (int) y - 27, this);

                g4.setColor(Color.BLACK);
                g4.drawString("FINISH", (int) x - 16, (int) y - 28);
                g4.fill(new Ellipse2D.Double(x - 4, y - 4, 8, 8));
                g4.setColor(Color.RED);
                g4.fill(new Ellipse2D.Double(x - 3, y - 3, 6, 6));
            }

            if (LP1 != null) {
                double x = LP1.getX() * scale + tx;
                double y = LP1.getY() * scale + ty;
                // g4.drawImage(img1, (int) x - 12, (int) y - 27, this);
                g4.setColor(Color.BLACK);
                // g4.drawString("You are HERE", (int) x - 32, (int) y - 30);
                g4.fill(new Ellipse2D.Double(x - 4, y - 4, 8, 8));
                g4.setColor(Color.RED);
                g4.fill(new Ellipse2D.Double(x - 3, y - 3, 6, 6));
            }

        }
    }
}
