package edu;

import java.util.ArrayList;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import org.xml.sax.*;

public class XMLRouteHandler extends DefaultHandler {

    private MapNode nd_obj;
    private MapWay way_obj;

    private boolean bNode = false;
    private boolean bWays = false;

    private ArrayList<MapWay> ways = new ArrayList<MapWay>();
    private ArrayList<MapNode> nodes = new ArrayList<MapNode>();
    private ArrayList<Integer> refList;

    public ArrayList<MapNode> getMapNodeList() {
        return nodes;
    }

    public ArrayList<MapWay> getMapWayList() {
        return ways;
    }

    @Override
    public void startDocument() throws SAXException {
        //System.out.println("SAX PARSER: ANFANG");

    }

    @Override
    public void endDocument() throws SAXException {
        //System.out.println("SAX PARSER: ENDE");
    }

    @Override
    public void startElement(String namespaceURI, String localname, String tagname, Attributes attributes) throws SAXException {
        if (tagname.equals("node")) {

            bNode = true;

            int id = Integer.parseInt(attributes.getValue(0));
            int x_wert = Integer.parseInt(attributes.getValue(1));
            int y_wert = Integer.parseInt(attributes.getValue(2));

            nd_obj = new MapNode(id, x_wert, y_wert);

            nodes.add(nd_obj);

        }
        if (tagname.equals("tag")) {

            if (bNode) {
                String key = attributes.getValue(0);
                String value = attributes.getValue(1);

                nodes.get(nodes.size() - 1).addTag(key, value);
            }
            if (bWays) {
                String key = attributes.getValue(0);
                String value = attributes.getValue(1);

                ways.get(ways.size() - 1).addTag(key, value);
            }
        }

        if (tagname.equals("way")) {

            bWays = true;
            refList = new ArrayList<Integer>();
            way_obj = new MapWay(refList);
            ways.add(way_obj);
        }

        if (tagname.equals("nd")) {
            ways.get(ways.size() - 1).addNd(Integer.parseInt(attributes.getValue(0)));
        }
    }

    @Override
    public void endElement(String namespaceURI, String localname, String tagname) {
        if (tagname.equals("node")) {
            bNode = false;

        }

        if (tagname.equals("way")) {
            bWays = false;
        }
    }
}
