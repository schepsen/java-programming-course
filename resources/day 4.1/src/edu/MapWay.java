package edu;

import java.util.ArrayList;
import java.util.Hashtable;

public class MapWay {
    
    private ArrayList<Integer> nd = new ArrayList<Integer>();
    private Hashtable<String, String> tags = new Hashtable<String, String>();
    
    public MapWay(ArrayList<Integer> nd) {
        this.setNd(nd);
    }

    public ArrayList<Integer> getNd() {
        return nd;
    }

    public void setNd(ArrayList<Integer> nd) {
        this.nd = nd;
    }
    
    public void addNd(Integer i){
        this.nd.add(i);
    }
    
    public void addTag(String key, String value){
        this.tags.put(key, value);
    }

    public Hashtable<String, String> getTags() {
        return tags;
    }

    public void setTags(Hashtable<String, String> tags) {
        this.tags = tags;
    }
}


