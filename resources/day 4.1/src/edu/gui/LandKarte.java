package edu.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.net.URL;
import java.util.ArrayList;
import javax.swing.JComponent;

/**
 * 
 * @author Nikolaj Schepsen
 */
public class LandKarte extends JComponent {

    private static final long serialVersionUID = 1L;
    private Image deutschland;
    // Array for choosed Cities
    public ArrayList<Ellipse2D> selectedItems = new ArrayList<Ellipse2D>();
    public ArrayList<Ellipse2D> testNode = new ArrayList<Ellipse2D>();
    // Array for Lines between choosed Cities
    public ArrayList<Line2D> lines = new ArrayList<Line2D>();
    public ArrayList<Line2D> testLine = new ArrayList<Line2D>();

    public LandKarte() {

        URL location = this.getClass().getResource("map.png");
        Toolkit tk = this.getToolkit();
        deutschland = tk.getImage(location);

    }

    @Override
    protected void paintComponent(Graphics g) {
        if (deutschland != null) {
            g.drawImage(deutschland, 0, 0, this);

        }

        // Male Kreise, wenn ein Start bzw. ein Endpunkt ausgewählt wurde


        // Male Linien, falls horhanden

        
        Graphics2D glt = (Graphics2D) g;
        for (Line2D l2 : testLine) {
            
            glt.setColor(Color.RED);
            glt.draw(l2);
        }
        
                Graphics2D g3 = (Graphics2D) g;
        for (Line2D l : lines) {
            
            g3.setColor(Color.BLACK);
            g3.draw(l);
        }
        
                Graphics2D g2 = (Graphics2D) g;
        for (Ellipse2D t : selectedItems) {
            g2.setColor(Color.RED);
            g2.draw(t);
            g2.fill(t);
        }
        
        Graphics2D gt = (Graphics2D) g;
        for (Ellipse2D t2 : testNode) {
            gt.setColor(Color.BLACK);
            gt.draw(t2);
            gt.fill(t2);
        }

    }
}
