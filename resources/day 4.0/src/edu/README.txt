Arbeitsblatt #4    

A1:

created classes (containers):
    * MapNode.java
    * MapWay.java
created classes (parser):
    * XMLRouteHandler.java

Die Funktionstuechtigleit des Parser kann man mit der Klasse "XMLRouteParser.java" geprueft werden.

Das GUI und darausfolgende A2 (Aufgabe 2):

    * MapGUI.java
        - Das Mian-Programm
    * Dijkstra.java
        - berechnet den kuerzesten Weg und gibt Objecte (MapNodes) zuueck
    * WeightedGraph.java
        - ein Graph, der aus den Objeckten (MapNode&MapWay) besteht und die Landkarte von Deutschland repraesentiert.


[HOWTO] FUNKTIONSWEISE

1.  Man waehlt mit einem Klick eine Stadt aus
1.1 Wenn eine Stadt im Umkreis von 5 / 10 km existiert, wird sie durch ein rotes Kreis hervorgehoben
2.  Man waehlt mit einrm Klick eine weitere Stadt aus
2.1 Wenn eine Stadt im Umkreis von 5 / 10 km existiert, wird sie durch ein rotes Kreis hervorgehoben
3.  Mit dem dritten Mausklick wird der kuerzeste Weg berechnet und graphisch dargestellt
4.  Mit dem Klick # 4 wird das Programm in den Urzustand zurueckgesetzt d.h. man kann von vorne beginnen



bearbeitet von Nikolaj Schepsen & Thi Huynh