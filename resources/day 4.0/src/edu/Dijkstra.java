package edu;

import java.util.ArrayList;

public class Dijkstra {

    public static int[] dijkstra(WeightedGraph G, int s) {
        final int[] dist = new int[G.getNumVertices()];
        final int[] pred = new int[G.getNumVertices()];
        final boolean[] visited = new boolean[G.getNumVertices()];

        for (int i = 0; i < dist.length; i++) {
            dist[i] = Integer.MAX_VALUE;
        }
        dist[s] = 0;

        for (int i = 0; i < dist.length; i++) {
            final int next = minVertex(dist, visited);
            visited[next] = true;

            final int[] n = G.neighbors(next);
            for (int j = 0; j < n.length; j++) {
                final int v = n[j];
                final int d = dist[next] + G.getWeight(next, v);
                if (dist[v] > d) {
                    dist[v] = d;
                    pred[v] = next;
                }
            }
        }
        return pred;
    }

    private static int minVertex(int[] dist, boolean[] v) {
        int x = Integer.MAX_VALUE;
        int y = -1;
        for (int i = 0; i < dist.length; i++) {
            if (!v[i] && dist[i] < x) {
                y = i;
                x = dist[i];
            }
        }
        return y;
    }

    // @SuppressWarnings({ "rawtypes", "unchecked" })
    public static ArrayList<MapNode> shortestPath(WeightedGraph G, int start, int end) {

        final int[] pred = Dijkstra.dijkstra(G, start);

        final ArrayList<MapNode> path = new ArrayList<MapNode>();
        int x = end;
        while (x != start) {
            path.add(0, G.getStadt(x));
            x = pred[x];
        }
        path.add(0, G.getStadt(start));
        return path;
    }

}
