package edu;

import java.util.Vector;

public class Treck {

    private String start;
    private String zielort;
    Vector<Esel> treck = new Vector<Esel>();
    private int aktuelleRunde;
    private int verblieben;
    private boolean isStarted;

    public Treck(String start, String zielort) {
        this.setStartOrt(start);
        this.setZielOrt(zielort);

    }

    public String getStartOrt() {
        return start;
    }

    public void setStartOrt(String start) {
        this.start = start;
    }

    public String getZielOrt() {
        return zielort;
    }

    public void setZielOrt(String zielort) {
        this.zielort = zielort;
    }

    public void setRunde(int round) {

        this.aktuelleRunde = round;
    }

    public int getRunde() {
        return this.aktuelleRunde;
    }

    public void setVerblieben(int km) {
        this.verblieben = km;
    }

    public int getVerblieben() {
        return this.verblieben;
    }

    public boolean isStarted() {
        return this.isStarted;
    }

    public void setStarted(boolean status) {
        this.isStarted = status;
    }

    public Vector<Esel> getEsel() {
        return treck;
    }

    public void addEsel(Esel name) {
        treck.add(name);
    }

    public void addLoad(int weight) {
        int mark = 0;
        int maximum = 0;

        for (int i = 0; i < treck.size(); i++) {
            if (treck.get(i).getPace() - treck.get(i).getLoad() > maximum) {
                mark = i;
                maximum = treck.get(i).getPace() - treck.get(i).getLoad();
            }
        }

        treck.get(mark).setLoad(weight);

    }

    public void entladen() {
        for (int i = 0; i < treck.size(); i++) {
            treck.get(i).setLoadZero(0);
        }
    }

    public int velocity() {

        if (treck.size() > 0) {
            int velocity = treck.get(0).getPace() - treck.get(0).getLoad();

            for (int i = 1; i < treck.size(); i++) {

                if (treck.get(i).getPace() - treck.get(i).getLoad() < velocity) {
                    velocity = treck.get(i).getPace() - treck.get(i).getLoad();
                }
            }
            return (velocity < 0) ? 0 : velocity;
        }
        return 0;
    }

    public void bewegen() {
    }

    public int getKoordinate(String ort, String koordinate) {

        int x = 0;
        int y = 0;

        if (ort.equals("Kiel")) {
            x = 353;
            y = 106;
        } else if (ort.equals("Hamburg")) {
            x = 346;
            y = 203;
        } else if (ort.equals("Schwerin")) {
            x = 444;
            y = 195;
        } else if (ort.equals("Bremen")) {
            x = 258;
            y = 258;
        } else if (ort.equals("Berlin")) {
            x = 601;
            y = 327;
        } else if (ort.equals("Hannover")) {
            x = 325;
            y = 348;
        } else if (ort.equals("Magdeburg")) {
            x = 464;
            y = 381;
        } else if (ort.equals("Potsdam")) {
            x = 576;
            y = 352;
        } else if (ort.equals("Dresden")) {
            x = 639;
            y = 514;
        } else if (ort.equals("Erfurt")) {
            x = 423;
            y = 532;
        } else if (ort.equals("Düsseldorf")) {
            x = 90;
            y = 482;
        } else if (ort.equals("Wiesbaden")) {
            x = 198;
            y = 632;
        } else if (ort.equals("Mainz")) {
            x = 197;
            y = 649;
        } else if (ort.equals("Saarbrücken")) {
            x = 96;
            y = 738;
        } else if (ort.equals("Stuttgart")) {
            x = 275;
            y = 803;
        } else if (ort.equals("München")) {
            x = 476;
            y = 880;
        }

        if (koordinate.equals("x")) {
            return x;
        } else {
            return y;
        }
    }

    public int getDistanz() {

        String A = this.getStartOrt();
        String B = this.getZielOrt();

        int distanz = (int) Math.sqrt(Math.pow(getKoordinate(B, "x") - getKoordinate(A, "x"), 2.0) + Math.pow(getKoordinate(B, "y") - getKoordinate(A, "y"), 2.0));

        return distanz;
    }

    public int restStrekeBerechnen() {

        int rest = getVerblieben() - velocity();

        return (rest < 0) ? 0 : rest;
    }
}
