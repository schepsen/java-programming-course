Aufgabe 1:

	- Classes:
		edu.Treck;
		edu.Esel;
		
	MainClass.java
	
Aufgabe 2 (JUnit-Test):
	
	Testfälle:
		test1 (velocity:20) 
		test2 (velocity:15)	
		test3 (velocity: 0)
		
Aufgabe 3:

	Jeder Esel kann zu genau einem Treck gehören: setStatus(Value: true oder false)
	Jeder Treck hat ein Start und ein Ziel: Attributte: StartOrt && ZielOrt. Mit getKoordinate(String Ort, String Koordinate) kann man die Position des Ortes bestimmen.
	Ein Treck kann gestartet werden: Button "STARTEN". Der Abstand zwischen den Städten berechtet die Methode getDistanz();
	Die Bewegung des Trecks erfolgt rundenbasiert: JA
	Ein gestarteter Treck kann nicht mehr ver¨andert werden: Ja, Buttons sind disabled
	Solange ein Treck noch nicht gestartet ist, kann er beladen werden: JA
	Außerdem sollen Esel hinzugefügt werden: JA
	
Aufgabe 4:

	-CLasses:
		to run edu.gui.TreckGUI;
	Alle Trecks und alle Esel mit Ihrem Status angezeigt werden: JA
	Die Möglichkeit bestehen zus¨atzliche Esel in den Bestand aufzunehmen: JA, Dafür gibt es ein Button "In den bestand aufnehmen" 
	Die Möglichkeit bestehen Trecks zusammenzustellen, zu beladen, zu starten: JA
	Ein Schalter: Dafür gibt es ein Button: "next Round";