package edu;

/**
 * PRG Praktikum (Day 1) Aufgabe 3
 * @author Nikolaj Schepsen
 */

public class A3 {

    static String[] A = { "eins", "zwei", "drei", "vier", "fuenf", "sechs", "sieben", "acht", "neun", "zehn", "elf", "zwoelf", "dreizehn", "vierzehn", "fuenfzehn", "sechzehn", "siebzehn", "achtzehn", "neunzehn" };
    static String[] B = { "zehn", "zwanzig", "dreissig", "vierzig", "fuenfzig", "sechzig", "siebzig", "achtzig", "neunzig" };

    public static void main(String[] args) {

        try {
            // Konvertieren args[] nach Integer
            int START = Integer.parseInt(args[0]);
            int STOP = Integer.parseInt(args[1]);
            // Prüfen, ob die eingegebenen Werte sinnvoll sind
            if (START <= STOP && STOP < 100) {

                for (int i = START; i <= STOP; i++) {
                    if (i < 20) {
                        // Fall 1: Druecken alle Zahlen, die kleiner als 20 sind
                        System.out.println(A[i - 1]);
                    } else if (i % 10 == 0) {
                        // Fall 2: Druecken alle Zahlen, die modulo 10 0 ergeben
                        System.out.println(B[i / 10 - 1]);
                    } else { // Fall 3: Rest
                        System.out.println(A[i % 10 - 1].replace("eins", "ein") + "und" + B[i / 10 - 1]);
                    }
                }
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            // Fehlermeldung
            System.out.println("Ungueltige Eingabe");
        }
    }
}