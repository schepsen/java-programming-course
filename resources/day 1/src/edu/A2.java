package edu;

/**
 * PRG Praktikum (Day 1) Aufgabe 2
 * @author Nikolaj Schepsen
 */

public class A2 {
    
    static String[] A = { "eins", "zwei", "drei", "vier", "fünf", "sechs", "sieben", "acht", "neun", "zehn", "elf", "zwölf", "dreizehn", "vierzehn", "fünfzehn", "sechzehn", "siebzehn", "achtzehn", "neunzehn" };
    static String[] B = { "zehn", "zwanzig", "dreißig", "vierzig", "fünfzig", "sechzig", "siebzig", "achtzig", "neunzig" };

    public static void main(String[] args) {
        for (int i = 1; i <= 99; i++) {
            if (i < 20) {
                // Fall 1: Druecken alle Zahlen, die kleiner als 20 sind
                System.out.println(A[i - 1]);
            } else if (i % 10 == 0) {
                // Fall 2: Druecken alle Zahlen, die modulo 10 0 ergeben
                System.out.println(B[i / 10 - 1]);
            } else {
                // Fall 3: Rest
                System.out.println(A[i % 10 - 1].replace("eins", "ein") + "und" + B[i / 10 - 1]);
            }
        }
    }
}
