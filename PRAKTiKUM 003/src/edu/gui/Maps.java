/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Map.java
 *
 * Created on 15.11.2011, 12:20:30
 */
package edu.gui;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.geom.Rectangle2D;
import java.net.URL;
import java.util.ArrayList;
import javax.swing.JComponent;

/**
 *
 * @author Fonduee
 */
public class Maps extends JComponent {

    private Image landkarte;
    public ArrayList<Rectangle2D> trecklist = new ArrayList<Rectangle2D>();

    /** Creates new form Map */
    public Maps() {

        URL location = this.getClass().getResource("map.png");
        Toolkit tk = this.getToolkit();
        landkarte = tk.getImage(location);        

    }

    @Override
    protected void paintComponent(Graphics g) {
        if (landkarte != null) {
            g.drawImage(landkarte, 0, 0, this);


        }

        Graphics2D g2 = (Graphics2D) g;
        for (Rectangle2D t : trecklist) {
            g2.draw(t);
        }

    }
}
