/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.gui;

import java.awt.geom.Rectangle2D;

/**
 *
 * @author Fonduee
 */
public class MapsFunctional extends Maps {

    public MapsFunctional() {
        super();

    }

    public void remove(Rectangle2D t) {

        if (t == null) {
            return;
        }

        // @Modell: Ändere deinen Zustand
        trecklist.remove(t);

        // @View: Ändere deine Anzeige
        repaint();
    }
}
