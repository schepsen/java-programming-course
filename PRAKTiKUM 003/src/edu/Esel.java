package edu;

public class Esel {

    private int pace;
    private int load;
    private String name;
    private String standort;
    private boolean inUse;

    public Esel(int EselPace, String EselName) {

        this.setPace(EselPace);
        this.setName(EselName);
    }

    public Esel(int EselPace, String EselName, String EselOrt) {

        this.setPace(EselPace);
        this.setName(EselName);
        this.setStandort(EselOrt);
    }

    public int getPace() {
        return pace;
    }

    public void setPace(int pace) {
        this.pace = pace;
    }

    public int getLoad() {
        return load;
    }

    public void setLoad(int load) {
        this.load = this.getLoad() + load;
    }

    public void setLoadZero(int load) {
        this.load = 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStandort() {
        return standort;
    }

    public void setStandort(String standort) {
        this.standort = standort;
    }

    public boolean getStatus() {
        return inUse;
    }

    public void setStatus(boolean status) {
        this.inUse = status;
    }
}
