package edu;

public class MainClass {

    public static void main(String[] args) {

        // Erstelle drei Esel
        Esel heinz = new Esel(30, "Heinz");
        Esel liesel = new Esel(25, "Liesel");
        Esel speedy = new Esel(40, "");

        // Erstelle den Treck (von "Hamburg" nach "München")
        Treck nordSued = new Treck("Hamburg", "München");

        nordSued.addEsel(heinz);
        nordSued.addEsel(liesel);
        nordSued.addEsel(speedy);

        System.out.println(nordSued.velocity());
        
        // Belade den Treck
        nordSued.addLoad(20);

        // Zeige die aktuelle G´keit des Trecks
        System.out.println(nordSued.velocity());
        
        nordSued.entladen();
        System.out.println(nordSued.velocity());

    }

}
