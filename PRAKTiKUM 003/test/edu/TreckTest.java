/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu;

import edu.Esel;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Fonduee
 */
public class TreckTest {

    public TreckTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void test1() {
        Esel heinz = new Esel(35, "Heinz");
        Esel liesel = new Esel(20, "Liesel");
        Esel speedy = new Esel(40, "Speedy");

        Treck nordSued = new Treck("Hamburg", "Munchen");

        nordSued.addEsel(heinz);
        nordSued.addEsel(liesel);
        nordSued.addEsel(speedy);

        nordSued.addLoad(20);
        System.out.print("Test 1: Testing ... ");
        assertEquals("Test 1", 20, nordSued.velocity());
        System.out.print("Done!\n");
    }

    @Test
    public void test2() {
        Esel heinz = new Esel(35, "Heinz");
        Esel liesel = new Esel(20, "Liesel");
        Esel speedy = new Esel(40, "Speedy");

        Treck nordSued = new Treck("Hamburg", "Munchen");

        nordSued.addEsel(heinz);
        nordSued.addEsel(liesel);
        nordSued.addEsel(speedy);

        nordSued.addLoad(25);
        System.out.print("Test 2: Testing ... ");
        assertEquals("Test 2", 15, nordSued.velocity());
        System.out.print("Done!\n");
    }

    @Test
    public void test3() {
        Esel heinz = new Esel(35, "Heinz");
        Esel liesel = new Esel(20, "Liesel");
        Esel speedy = new Esel(40, "Speedy");

        Treck nordSued = new Treck("Hamburg", "Munchen");

        nordSued.addEsel(heinz);
        nordSued.addEsel(liesel);
        nordSued.addEsel(speedy);

        nordSued.addLoad(100);
        System.out.print("Test 3: Testing ... ");
        assertEquals("Test 3", 0, nordSued.velocity());
        System.out.print("Done!\n");
    }
}
